# Construcción manual de una red entre contenedores

La composición de este repositorio se compone de varios niveles de implementación de red sobre contenedores docker. Dispone de carpetas numeradas cada una con dos o tres fichers dentro, esto corresponde con cada punto del ejercicio. Cada carpeta tiene un fichero _run.sh_ y _clean.sh_, run despliega los contenedores y la red correspondiente al numero de ejercicio que nombra la carpeta, mientras que clean mata los contenedores y elimina la configuración de red producida por _run.sh_.

En los apartados 1. y 3. además se incluye una imagen docker correspondiente a la que se usara para el despliegue.

- La imagen de 1. es un contenedor ubuntu con iproute2 instalado (con el objetivo de usar ping). Se usara en el punto 1. y 2.
- La imagen de 3. es un contenedor ubuntu con iproute2, openssh, apache2 instalado (con el objetivo de desplegar un servicio ssh y web a mayores de lo incluido en la imagen de 1.). Se usara en el punto 3.

En todos los puntos, para poder realizar el test correctamente se ha de ejecutar el script _run.sh_ del punto correspondiente. 


## Punto 1: Comunicación entre contenedores

Una vez creados los contenedores sin red propia de docker, creamos una interfaz bridge el cual sera conectado a cada contenedor como 
vemos en la *figura1*.
Con el objetivo de que no se vea en el propio host, lo incrustamos en su propio namespace (ubuntu-net).

Para expandir esta red solo tendriamos que añadir otro contenedor y conectarlo al bridge mediante un cable ethernet (veth).

![Figura1](images/1.png)

### Test

Para probar que funciona, hacemos ping desde el primer contenedor al segundo:

`sudo ip netns exec ubuntu1 ping 192.168.2.11` 




## Punto 2: Comunicación con el host

Para realizar este apartado, eliminamos el namespace que contenia a la red en el apartado anterior (ubuntu-net), lo cual permite la conexión directa con el host como se muestra en la *figura2*

Esto también se podría haber logrado mediante una conexión ethernet del namespace creado en 1. al host.

![Figura2](images/2.png)

### Test

Para comprobar que funciona, hacemos ping al primer contenedor desde el host:

`ping 192.168.2.10`


## Punto 3: Comunicación con nodos alcanzables desde el host

Para realizar este apartado, incluimos reglas de redirección de puertos en la tabla nat de iptables (Ademas de reglas para permitir el trafico al destino seleccionado).

`sudo iptables -t nat -A PREROUTING -p tcp -i $default_iface --dport 80 -j DNAT --to-destination 192.168.2.10:80`

`sudo iptables -A FORWARD -p tcp -d 192.168.2.10 --dport 80 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT`

`sudo iptables -t nat -A POSTROUTING ! -s 127.0.0.1 -j MASQUERADE`

Tener en cuenta que si ejecutamos el script de creacion, en este se incluye la orden para inicial el servicio de iptables para distribuciones basadas en arch (`systemctl start iptables.service`), si se usa otra distribución, se debe cambiar dicho comando por uno equivalente.

![Figura3](images/3.png)

### Test

Para comprobar que funciona, metemos en un navegador de un host distinto la ip publica del PC donde se ha ejecutado el script (deben estar en la misma subred).

## Punto 4: Comunicación entre nodos diferentes desde la misma subred


Para realizar este apartado realizo una versión simple que implementa un enrutado de paquetes hacia el otro host (desde cada host). Para ello en el script, al final del todo se usa el siguiente comando (que se debera de rellenar adecuadamente para que funcione).

`sudo ip route add 192.168.3.0/24 via (IP DE HOST B)`

Comando que enruta los paquetes del Host A a los del B. La estructura de la red se puede obserbar en la siguiente figura:

![Figura4](images/4.png)

Para generalizar dicha conexión se deberia de disponer de todas las ip publicas (y ip de la interfaces de subred creadas) de los dispositivos, añadiendo a cada host un comando de enrutado por cada otro host de la forma: 

`sudo ip route add (interfaz1) via (IP DEL HOST DE INTERFAZ)`

`sudo ip route add (interfaz2) via (IP DEL HOST DE INTERFAZ)`

`sudo ip route add (interfaz3) via (IP DEL HOST DE INTERFAZ)`


### Test

Para comprobar que funciona, hacemos ping desde un contenedor de Host A a Host B usando la ip del contenedor.

Desde Host A:

`sudo ip netns exec ubuntu1 ping 192.168.3.10`

