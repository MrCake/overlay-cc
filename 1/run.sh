#!/bin/bash


# Create docker containers without network
docker run --rm -dit --network none --name ubuntu1 ubuntu-ip
docker run --rm -dit --network none --name ubuntu2 ubuntu-ip
docker run --rm -dit --network none --name ubuntu3 ubuntu-ip

#Make docker containers network namespace visible
sudo mkdir -p /var/run/netns

pid1="$(docker inspect ubuntu1 -f '{{.State.Pid}}')"
pid2="$(docker inspect ubuntu2 -f '{{.State.Pid}}')"
pid3="$(docker inspect ubuntu3 -f '{{.State.Pid}}')"

sudo ln -sf /proc/$pid1/ns/net /var/run/netns/ubuntu1
sudo ln -sf /proc/$pid2/ns/net /var/run/netns/ubuntu2
sudo ln -sf /proc/$pid3/ns/net /var/run/netns/ubuntu3

#Create cluster namespace (So it cant be seen by host)
sudo ip netns add ubuntu-net

#Create a bridge
sudo ip netns exec ubuntu-net ip link add br0 type bridge

#Associate containers with bridge
sudo ip netns exec ubuntu-net ip link add veth1 type veth peer name br-veth1
sudo ip netns exec ubuntu-net ip link add veth2 type veth peer name br-veth2
sudo ip netns exec ubuntu-net ip link add veth3 type veth peer name br-veth3

sudo ip netns exec ubuntu-net ip link set veth1 netns ubuntu1
sudo ip netns exec ubuntu-net ip link set veth2 netns ubuntu2
sudo ip netns exec ubuntu-net ip link set veth3 netns ubuntu3

sudo ip netns exec ubuntu-net ip link set br-veth1 master br0
sudo ip netns exec ubuntu-net ip link set br-veth2 master br0
sudo ip netns exec ubuntu-net ip link set br-veth3 master br0

#Set ip and service
sudo ip netns exec ubuntu-net ip netns exec ubuntu1 ip addr add 192.168.1.10/24 dev veth1
sudo ip netns exec ubuntu-net ip netns exec ubuntu2 ip addr add 192.168.1.11/24 dev veth2
sudo ip netns exec ubuntu-net ip netns exec ubuntu3 ip addr add 192.168.1.12/24 dev veth3

sudo ip netns exec ubuntu-net ip addr add 192.168.1.5/24 brd + dev br0

#Turn up
sudo ip netns exec ubuntu-net ip link set br0 up
sudo ip netns exec ubuntu-net ip link set br-veth1 up
sudo ip netns exec ubuntu-net ip link set br-veth2 up
sudo ip netns exec ubuntu-net ip link set br-veth3 up
sudo ip netns exec ubuntu-net ip netns exec ubuntu1 ip link set veth1 up
sudo ip netns exec ubuntu-net ip netns exec ubuntu2 ip link set veth2 up
sudo ip netns exec ubuntu-net ip netns exec ubuntu3 ip link set veth3 up


#Disable iptables on bridge
sudo bash -c "echo 0 > /proc/sys/net/bridge/bridge-nf-call-iptables"

