#!/bin/bash


# Create docker containers without network
docker run --rm -dit --network none --name ubuntu1 ubuntu-ip-web 
docker run --rm -dit --network none --name ubuntu2 ubuntu-ip-web 
docker run --rm -dit --network none --name ubuntu3 ubuntu-ip-web 

docker exec ubuntu1 service apache2 start
docker exec ubuntu2 service apache2 start
docker exec ubuntu3 service apache2 start

docker exec ubuntu1 /etc/init.d/ssh restart
docker exec ubuntu2 /etc/init.d/ssh restart
docker exec ubuntu3 /etc/init.d/ssh restart


#Make docker containers network namespace visible
sudo mkdir -p /var/run/netns

pid1="$(docker inspect ubuntu1 -f '{{.State.Pid}}')"
pid2="$(docker inspect ubuntu2 -f '{{.State.Pid}}')"
pid3="$(docker inspect ubuntu3 -f '{{.State.Pid}}')"

sudo ln -sf /proc/$pid1/ns/net /var/run/netns/ubuntu1
sudo ln -sf /proc/$pid2/ns/net /var/run/netns/ubuntu2
sudo ln -sf /proc/$pid3/ns/net /var/run/netns/ubuntu3


#Create a bridge
sudo ip link add br0 type bridge

#Associate containers with bridge
sudo ip link add veth1 type veth peer name br-veth1
sudo ip link add veth2 type veth peer name br-veth2
sudo ip link add veth3 type veth peer name br-veth3

sudo ip link set veth1 netns ubuntu1
sudo ip link set veth2 netns ubuntu2
sudo ip link set veth3 netns ubuntu3

sudo ip link set br-veth1 master br0
sudo ip link set br-veth2 master br0
sudo ip link set br-veth3 master br0

#Set ip and service
sudo ip netns exec ubuntu1 ip addr add 192.168.2.10/24 dev veth1
sudo ip netns exec ubuntu2 ip addr add 192.168.2.11/24 dev veth2
sudo ip netns exec ubuntu3 ip addr add 192.168.2.12/24 dev veth3

sudo ip addr add 192.168.2.1/24 brd + dev br0

#Turn up
sudo ip link set br0 up
sudo ip link set br-veth1 up
sudo ip link set br-veth2 up
sudo ip link set br-veth3 up
sudo ip netns exec ubuntu1 ip link set veth1 up
sudo ip netns exec ubuntu2 ip link set veth2 up
sudo ip netns exec ubuntu3 ip link set veth3 up


#Disable iptables on bridge
sudo bash -c "echo 0 > /proc/sys/net/bridge/bridge-nf-call-iptables"

#Activate iptables service 
systemctl start iptables

default_iface=$(route | grep '^default' | grep -o '[^ ]*$')
#Port forward and remove filter (Only forwards web page)
sudo iptables -t nat -A PREROUTING -p tcp -i $default_iface --dport 80 -j DNAT --to-destination 192.168.2.10:80
sudo iptables -t nat -A PREROUTING -p tcp -i $default_iface --dport 81 -j DNAT --to-destination 192.168.2.11:80
sudo iptables -t nat -A PREROUTING -p tcp -i $default_iface --dport 82 -j DNAT --to-destination 192.168.2.12:80


sudo iptables -A FORWARD -p tcp -d 192.168.2.10 --dport 80 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A FORWARD -p tcp -d 192.168.2.11 --dport 80 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A FORWARD -p tcp -d 192.168.2.12 --dport 80 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

#SSH (ssh user@pc-ip) pass: duero
sudo iptables -t nat -A PREROUTING -p tcp -i $default_iface --dport 22 -j DNAT --to-destination 192.168.2.10:22
sudo iptables -t nat -A PREROUTING -p tcp -i $default_iface --dport 23 -j DNAT --to-destination 192.168.2.11:22
sudo iptables -t nat -A PREROUTING -p tcp -i $default_iface --dport 24 -j DNAT --to-destination 192.168.2.12:22

sudo iptables -A FORWARD -p tcp -d 192.168.2.10 --dport 22 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A FORWARD -p tcp -d 192.168.2.11 --dport 22 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A FORWARD -p tcp -d 192.168.2.12 --dport 22 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

#(Use SNAT instead of MASQUERADE if static ip assignment)
sudo iptables -t nat -A POSTROUTING ! -s 127.0.0.1 -j MASQUERADE



