#!/bin/bash

#sudo ip route del 192.168.x.0/24 via xxx.xxx.xxx.xxx 

#Default iptables (https://wiki.archlinux.org/index.php/Iptables)

sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -t raw -F
sudo iptables -t raw -X
sudo iptables -t security -F
sudo iptables -t security -X
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT


#Enable default iptables on bridge
sudo bash -c "echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables"


#Delete bridge
sudo  ip link delete br0

#Remove visible network namespaces
sudo rm /var/run/netns/ubuntu1
sudo rm /var/run/netns/ubuntu2
sudo rm /var/run/netns/ubuntu3

#Remove created containers
docker kill ubuntu1
docker kill ubuntu2
docker kill ubuntu3

