#!/bin/bash
#sudo ip link delete veth1
#sudo ip link delete veth2
#sudo ip link delete veth3

#Enable default iptables on bridge
sudo bash -c "echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables"


#Delete bridge
sudo  ip link delete br0

#Remove visible network namespaces
sudo rm /var/run/netns/ubuntu1
sudo rm /var/run/netns/ubuntu2
sudo rm /var/run/netns/ubuntu3

#Remove created containers
docker kill ubuntu1
docker kill ubuntu2
docker kill ubuntu3

